# variable vpc_name {}
# variable subnet_name_01 {
#   description = "Nome que será adicionado ao recurso"
# }
# variable subnet_name_02 {
#   description = "Nome que será adicionado ao recurso"
# }
# variable subnet_name_03 {
#   description = "Nome que será adicionado ao recurso"
# }
# variable subnet_name_04 {
#   description = "Nome que será adicionado ao recurso"
# }
# variable subnet_name_05 {
#   description = "Nome que será adicionado ao recurso"
# }
# variable subnet_name_06 {
#   description = "Nome que será adicionado ao recurso"
# }
# variable subnet_name_07 {
#   description = "Nome que será adicionado ao recurso"
# }
# variable subnet_name_08 {
#   description = "Nome que será adicionado ao recurso"
# }
# variable name_db_instance {}
variable db_name {}
variable allocated_storage {}
variable engine {}
variable engine_version {}
variable instance_class {}
variable license_model {}
variable identifier {}
variable username {}
variable password {}
# variable db_subnet_group_name {}
variable parameter_group_name {}
variable multi_az {}
# variable vpc_security_group_ids {
#   type        = list
# }
variable storage_type {}
variable backup_retention_period {}
variable availability_zone {}
variable option_group_name {}
variable skip_final_snapshot {}
# variable environment_instance {}
variable owner_name {}
variable owner_mail {}
variable service_instance {}
# variable name_subnet {
#   description = ""
# }
# variable name_prefix_subnet {
#   description = ""
# }
variable description {
  description = ""
}
variable subnet_ids {
  type        = list
}
# variable environment_subnet {
#   description = ""
# }
# variable name_prefix_sg {
#   description = "Nome que será adicionado ao recurso"
#   }
# variable name_sg {
#   description = "Nome que será adicionado ao recurso"
# }
variable vpc_id {
  description = "ID da VPC usada na subnet"
  }
# variable environment_sg {
#   description = "Nome completo do ambiente que o resurso irá existir"
# }
variable service_sg {
  description = ""
}

variable rules_ingress {
  type = map(object({ description = string, protocol = string, to_port = string, cidr_blocks = list(string) }))
}
variable rules_egress {
  type = map(object({ description = string, protocol = string, to_port = string, cidr_blocks = list(string) }))
}
variable env {}