resource "aws_db_instance" "db" {
  engine                  = var.engine
  engine_version          = var.engine_version
  instance_class          = var.instance_class
  license_model           = var.license_model
  allocated_storage       = var.allocated_storage
  identifier              = var.identifier
  name                    = var.db_name
  username                = var.username
  password                = var.password
  db_subnet_group_name    = aws_db_subnet_group.db_subnet.name
  parameter_group_name    = var.parameter_group_name
  multi_az                = var.multi_az
  vpc_security_group_ids  = [aws_security_group.security_group.id,]
  storage_type            = var.storage_type
  backup_retention_period = var.backup_retention_period
  availability_zone       = var.availability_zone
  option_group_name       = var.option_group_name
  skip_final_snapshot     = var.skip_final_snapshot
  tags = {
    # Environment        = var.environment_instance
    Owner_Name         = var.owner_name
    Owner_Mail         = var.owner_mail
    Service            = var.service_instance
  }
}