resource "aws_db_subnet_group" "db_subnet" {
  name_prefix = format("%s_%s_",var.env,var.db_name)
  description = var.description
  subnet_ids  = var.subnet_ids

  tags = {
    Name        = format("%s_%s",var.env,var.db_name)
    # environment = var.environment_subnet
  }
}