locals {
    env=var.env

    region = {
        "dev"="us-east-1"
        "stg"="us-east-1"
        "infra"="us-east-1"
        "prd"="sa-east-1"
    }

    env_large = {
        "dev"="development"
        "stg"="stage"
        "infra"="infraestrutura"
        "prd"="producao"

    }
}