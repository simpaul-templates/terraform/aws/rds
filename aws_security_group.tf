resource "aws_security_group" "security_group" {
  name_prefix = format("%s_%s_",var.env,var.db_name)
  vpc_id      = var.vpc_id

  ingress {
    description = "Accesso full para todos do security group"
    from_port   = 0
    to_port     = 65535
    protocol    = "tcp"
    self        = "true"
  }

  dynamic "ingress" {
    for_each = var.rules_ingress
    content {
      description = ingress.value["description"]
      from_port   = ingress.key
      to_port     = ingress.value["to_port"]
      protocol    = ingress.value["protocol"]
      cidr_blocks = ingress.value["cidr_blocks"]
    }
  }

  dynamic "egress" {
    for_each = var.rules_egress
    content {
      description = egress.value["description"]
      from_port   = egress.key
      to_port     = egress.value["to_port"]
      protocol    = egress.value["protocol"]
      cidr_blocks = egress.value["cidr_blocks"]
    }
  }

  tags = {
    Name        = format("%s_%s",var.env,var.db_name)
    # Environment = var.environment_sg
    Service     = var.service_sg
  }

  lifecycle {
    create_before_destroy = true
  }
}